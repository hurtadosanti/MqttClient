//
// Created by Santiago Hurtado
//

#include "mqtt/async_client.h"
#include "LiteralCallback.h"

int main(int argc, char *argv[]) {

    const std::string clientId("cloud_client_2");
    std::string topicName("test");
    const int qos = 1;

    std::string username;
    std::string password;
    std::string serverAddress;

    if (argc > 3) {
        username = argv[1];
        password = argv[2];
        serverAddress = argv[3];
    } else {
        std::cerr << "Broker Settings should be set" << std::endl;
        return 1;
    }

    try {

        mqtt::async_client cli(serverAddress, clientId);
        mqtt::connect_options connectOptions(username, password);
        mqtt::ssl_options sslOptions;
        connectOptions.set_ssl(sslOptions);

        LiteralCallback cb(cli, topicName);
        cli.set_callback(cb);

        auto connection = cli.connect(connectOptions, nullptr, cb);
        connection->wait();

        mqtt::topic topic(cli, topicName, qos);

        auto publishToken = topic.publish("Cloud Client Testing");
        std::cout<<"Message send"<<std::endl;
        publishToken->wait();
        std::cout<<"Message received"<<std::endl;
        cli.disconnect()->wait();
    }
    catch (const mqtt::exception &exc) {
        std::cerr << exc << std::endl;
        return 2;
    }

    return 0;
}


