//
// Created by Santiago Hurtado
//

#include <utility>

#include "mqtt/async_client.h"

/**
 * Callback that subscribe to read literal text messages
 */
class LiteralCallback : public virtual mqtt::callback,
                        public virtual mqtt::iaction_listener {
private:
    mqtt::async_client &client;
    const std::string topic;
public:
    LiteralCallback(mqtt::async_client &client, std::string &topic) : client(client), topic(topic) {
    }

    void connected(const mqtt::string &string) override {
        callback::connected(string);
        mqtt::subscribe_options options;
        mqtt::properties properties;
        client.subscribe(topic, 0, options, properties);
        std::cout << "Subscribed to topic"<<topic<< std::endl;
    };

    void message_arrived(mqtt::const_message_ptr message) override {
        callback::message_arrived(message);
        std::cout << "message arrived on topic: " << message->get_topic() << ", and message: " << message->get_payload()
                  << std::endl;

    };

    void on_failure(const mqtt::token &asyncActionToken) override {
        std::cout << "on failure" << std::endl;
        if (asyncActionToken.get_message_id() != 0)
            std::cout << " for token: [" << asyncActionToken.get_message_id() << "]" << std::endl;
        std::cout << std::endl;

    }

    void on_success(const mqtt::token &asyncActionToken) override {
        std::cout << "on success" << std::endl;
    }


    ~LiteralCallback() override = default;
};
