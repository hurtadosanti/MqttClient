# Mqtt CPP Client
Post description: https://advt3.com/posts/mqtt_cpp_client/

## Getting started

### Ubuntu 22.04
1. Install the compilation essentials
    ```
    sudo apt install build-essential cmake git pkg-config gcc g++ libssl-dev
    ```
2. Clone build and install the Eclipse Paho C Client Library for the MQTT Protocol.

    ```
    git clone https://github.com/eclipse/paho.mqtt.c.git
    cd paho.mqtt.c
    cmake -Bbuild -H. -DPAHO_ENABLE_TESTING=OFF -DPAHO_BUILD_STATIC=ON -DPAHO_WITH_SSL=ON -DPAHO_HIGH_PERFORMANCE=ON
    sudo cmake --build build/ --target install
    sudo ldconfig
    ```
3. Clone, build and install the Eclipse Paho MQTT C++ Client Library.
    ```
    cd ~/software
    git clone https://github.com/eclipse/paho.mqtt.cpp
    cd paho.mqtt.cpp
    cmake -Bbuild -H. -DPAHO_BUILD_STATIC=ON -DPAHO_BUILD_DOCUMENTATION=FALSE -DPAHO_BUILD_SAMPLES=FALSE
    sudo cmake --build build/ --target install
    sudo ldconfig
    ```
4. Build this project with CMake
   ```
   mkdir build
   cd build
   cmake ../
   make
   ```
5. Setup a [HiveMQ Cloud broker](https://console.hivemq.cloud)  
6. Run the binary with the three parameters from the HiveMQ Cloud settings:
   ```
   mqtt_client <username> <password> ssl://<url>
   ```

## License

MIT License 

Copyright (c) 2023 Santiago Hurtado